import midi from "midi";

var output = new midi.Output();
var input = new midi.Input();
console.log(input);
// var specialRequestIds = {
//     0x0F: 'SET_TEST_MODE'
// }

var midiMessages = {
  noteOff: {
    code: 0x80,
    bytes: 2,
  },
  noteOn: {
    code: 0x90,
    bytes: 2,
  },
  afterTouchPoly: {
    code: 0xa0,
    bytes: 2,
  },
  // NOTE: this is also the key for Channel Mode Messages
  controlChange: {
    code: 0xb0,
    bytes: 2,
  },
  programChange: {
    code: 0xc0,
    bytes: 1,
  },
  afterTouchChannel: {
    code: 0xd0,
    bytes: 1,
  },
  pitchBend: {
    code: 0xe0,
    bytes: 1,
  },
  systemExclusive: {
    code: 0xf0,
    bytes: -1, // Representing arbitrary number
  },
  timeCodeQuarterFrame: {
    code: 0xf1,
    bytes: 1,
  },
  songPosition: {
    code: 0xf2,
    bytes: 2,
  },
  songSelect: {
    code: 0xf3,
    bytes: 1,
  },
  tuneRequest: {
    code: 0xf6,
    bytes: 0,
  },
  clock: {
    code: 0xf8,
    bytes: 0,
  },
  start: {
    code: 0xfa,
    bytes: 0,
  },
  continue: {
    code: 0xfb,
    bytes: 0,
  },
  stop: {
    code: 0xfc,
    bytes: 0,
  },
  activeSensing: {
    code: 0xfe,
    bytes: 0,
  },
  reset: {
    code: 0xff,
    bytes: 0,
  },
};
/* eslint no-unused-vars: ["error", { "args": "none" }] */
function* midiMessageIterator(start = 0, end = Object.keys(midiMessages).length -1, step = 1) {
    let iterationCount = 0;
    for (var messageType in midiMessages) {
        iterationCount ++
        yield midiMessages[messageType];
    }
    return iterationCount;
}
export default {
  sendMidiData(port, data) {
    console.log("Sending data", data)
    output.sendMessage(data);
  },
  sleep(milliseconds) {
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  },
  runTest() {
    output.openPort(0);

    for (var messageType in midiMessages) {
      //   if (midiMessages.hasOwnProperty(messageType)) {
      if (midiMessages[messageType]["bytes"] === 0) {
        console.log(`Sending message of type ${messageType}`);
      // [0xF0, 0x00, 0x4d, 0x4b, 0x00, 0x00, 0x0F, 0xF7]);
        this.sendMidiData(0, [0xF0, 0x00, 0x4d, 0x4b, 0x00, 0x00, 0x0F, 0xF7])//[midiMessages[messageType].code]);
        break;
      }
      // }
      // console.log(messageType + " -> " + midiMessages[messageType]);
    }
  },
  parseMessage(data) {
    if (data === undefined) {
      return;
    }
    for (var messageType in midiMessages) {
      if (midiMessages[messageType].code === data[0]) {
        console.log(`Received message of type ${messageType} with data ${data}`);
      }
    }

    // SysEx
    // if(data[0] === 0xF0){
    //     // See https://gitlab.com/nikolai.orheim/midi-knife/-/wikis/Sysex-Messages
    //     // for information about sysex structure
    //     // Parse the manufacturer ID
    //     let manufacturerId = '';
    //     for (var i=1; i <= 3; i++) {
    //         manufacturerId += String.fromCharCode(data[i]);
    //     }
    //     console.log(`Got sysex message from ${manufacturerId}, with data ${data}`)
    // }
  },
  setupLogging(port) {
    input.openPort(port);
    input.ignoreTypes(false, false, false);
    input.on("message", (_, message) => {
        this.parseMessage(message);
        let next = midiMessageIterator().next();
        console.log(next);
        if (!next['done']){
            if(next.value.bytes === 0) {
              this.sendMidiData(0, [next.value.code])
            } else if (next.value.bytes === 1){
              this.sendMidiData(0, [next.value.code, 1])
            } else if (next.value.bytes === 2){
              this.sendMidiData(0, [next.value.code, 1, 2])
            } else { // SysEx
              this.sendMidiData(0, [next.value.code, 0, 0x4d, 0x4b, 0,0,1,0xF7])
            }
        }
      //   console.log(`d: ${deltaTime}, m: ${this.parseMessage(message)}`);
    });
  },
};
