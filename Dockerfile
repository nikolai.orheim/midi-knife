FROM ubuntu:18.04
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
# Opencv dependencies
RUN apt update
RUN apt -y install python3-pip
RUN pip3 install platformio 
# This takes the most time, everything else
# can be controlled from platformio.ini
RUN platformio platform install teensy