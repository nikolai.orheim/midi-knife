#include <Arduino.h>

void handleNoteOn(byte channel, byte note, byte velocity);
void handleNoteOff(byte channel, byte note, byte velocity);
void handleAfterTouchPoly(byte channel, byte note, byte velocity);
void handleControlChange(byte channel, byte control, byte value);
void handleProgramChange(byte channel, byte program);
void handleAfterTouchChannel(byte channel, byte pressure);
void handlePitchChange(byte channel, int pitch);
void handleSystemExclusiveChunk(const byte *data, uint16_t length, bool last);
void handleTimeCodeQuarterFrame(byte data);
void handleSongPosition(uint16_t beats);
void handleSongSelect(byte songNumber);
void handleTuneRequest();
void handleClock();
void handleStart();
void handleContinue(); 
void handleStop();
void handleActiveSensing();
void handleSystemReset();
void handleRealTimeSystem(uint8_t realtimebyte);
