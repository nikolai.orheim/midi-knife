#include <Arduino.h>
#include <sysExMessages.h>
// All midi signals share the same
byte type, channel, data1, data2, cable;

void handleNoteOn(byte channel, byte note, byte velocity)
{
}

void handleNoteOff(byte channel, byte note, byte velocity)
{
}

void handleAfterTouchPoly(byte channel, byte note, byte velocity)
{
}

void handleControlChange(byte channel, byte control, byte value)
{
}

void handleProgramChange(byte channel, byte program)
{
}

void handleAfterTouchChannel(byte channel, byte pressure)
{
}

void handlePitchChange(byte channel, int pitch)
{
}

void handleSystemExclusiveChunk(const byte *data, uint16_t length, bool last)
{
    // TODO: Currently ignores the last variable, need to create a better implementation
    // if larger sysex messages are going to be used
    cable = usbMIDI.getCable(); // which virtual cable with MIDIx16, 0-15
    sysExPackageIn inData;
    sysExPackageOut outData;
    // Copy data to the struct
    memcpy(&inData, data, sizeof(inData));
    // Copy the 3 first bytes (manufacturer ID), not including header
    memcpy(&outData, &data[1], 3);
    if (strcmp((char *)inData.manufacturer_id, "MK"))
    {
        switch (inData.special_request)
        {
        case 0x0F:
            outData.message_status = 0x01;
            outData.message_part = inData.message_part;
            outData.special_request = inData.special_request;
            usbMIDI.sendSysEx(sizeof(outData), (unsigned char *)&outData, false, cable);
        }
    }
}

void handleTimeCodeQuarterFrame(byte data)
{
}

void handleSongPosition(uint16_t beats)
{
}

void handleSongSelect(byte songNumber)
{
}

void handleTuneRequest()
{
}

void handleClock()
{
}

void handleStart()
{
}

void handleContinue()
{
}

void handleStop()
{
}

void handleActiveSensing()
{
}

void handleSystemReset()
{
}

void handleRealTimeSystem(uint8_t realtimebyte)
{
}
