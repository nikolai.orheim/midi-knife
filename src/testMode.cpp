
#include <testMode.h>
#include <sysExMessages.h>

void midiPingPong(void) {
  // Test mode for the MIDI knife
  // This responds with the same message that was sent 
  // (on the same cable), until a SysEx message with
  // message "MK TEST STOP" is sent
  byte type, channel, data1, data2, cable;

  // fetch the MIDI message, defined by these 5 numbers (except SysEX)
  //
  type = usbMIDI.getType();       // which MIDI message, 128-255
  channel = usbMIDI.getChannel(); // which MIDI channel, 1-16
  data1 = usbMIDI.getData1();     // first data byte of message, 0-127
  data2 = usbMIDI.getData2();     // second data byte of message, 0-127
  cable = usbMIDI.getCable();     // which virtual cable with MIDIx8, 0-7

  // uncomment if using multiple virtual cables
  //Serial.print("cable ");
  //Serial.print(cable, DEC);
  //Serial.print(": ");

  // print info about the message
  //
  switch (type) {
    case usbMIDI.NoteOff: // 0x80
      usbMIDI.sendNoteOff(data1, data2, channel, cable);
      break;

    case usbMIDI.NoteOn: // 0x90
      usbMIDI.sendNoteOn(data1, data2, channel, cable);
      // Serial.print("Note On, ch=");
      // Serial.print(channel, DEC);
      // Serial.print(", note=");
      // Serial.print(data1, DEC);
      // Serial.print(", velocity=");
      // Serial.println(data2, DEC);
      break;

    case usbMIDI.AfterTouchPoly: // 0xA0
      usbMIDI.sendAfterTouchPoly(data1, data2, channel, cable);
      // Serial.print("AfterTouch Change, ch=");
      // Serial.print(channel, DEC);
      // Serial.print(", note=");
      // Serial.print(data1, DEC);
      // Serial.print(", velocity=");
      // Serial.println(data2, DEC);
      break;

    case usbMIDI.ControlChange: // 0xB0
      usbMIDI.sendControlChange(data1, data2, channel, cable);
      // Serial.print("Control Change, ch=");
      // Serial.print(channel, DEC);
      // Serial.print(", control=");
      // Serial.print(data1, DEC);
      // Serial.print(", value=");
      // Serial.println(data2, DEC);
      break;

    case usbMIDI.ProgramChange: // 0xC0
      usbMIDI.sendProgramChange(data1, channel, cable);
      // Serial.print("Program Change, ch=");
      // Serial.print(channel, DEC);
      // Serial.print(", program=");
      // Serial.println(data1, DEC);
      break;

    case usbMIDI.AfterTouchChannel: // 0xD0
      usbMIDI.sendAfterTouch(data1, channel, cable);
      // Serial.print("After Touch, ch=");
      // Serial.print(channel, DEC);
      // Serial.print(", pressure=");
      // Serial.println(data1, DEC);
      break;

    case usbMIDI.PitchBend: // 0xE0
      usbMIDI.sendPitchBend(data1 + data2 * 128, channel, cable);
      // Serial.print("Pitch Change, ch=");
      // Serial.print(channel, DEC);
      // Serial.print(", pitch=");
      // Serial.println(data1 + data2 * 128, DEC);
      break;

    case usbMIDI.SystemExclusive: {
      // 0xF0
      // Messages larger than usbMIDI's internal buffer are truncated.
      // To receive large messages, you *must* use the 3-input function
      // handler.  See InputFunctionsComplete for details.
      // usbMIDI.sendPitchBend(123, 0, cable);
      sysExPackageIn inData;
      sysExPackageOut outData;
      // Get the pointer to the data array
      uint8_t * inBuffer = usbMIDI.getSysExArray();
      // Copy it to a local pointer
      memcpy (&inData, inBuffer, sizeof(inData));
      // Copy the 3 first bytes (manufacturer ID), not including header
      memcpy (&outData, &inBuffer[1], 3);
      if(strcmp((char *)inData.manufacturer_id, "MK")){
        switch(inData.special_request){
          case 0x0F:
            outData.message_status = 0x01;
            outData.message_part = inData.message_part;
            outData.special_request = inData.special_request;
            usbMIDI.sendSysEx(sizeof(outData), (unsigned char*) &outData, false, cable);
        }
      }
      break;
    }

    case usbMIDI.TimeCodeQuarterFrame: // 0xF1
      usbMIDI.sendTimeCodeQuarterFrame(data1 >> 4, data1 & 15, cable);
      // Serial.print("TimeCode, index=");
      // Serial.print(data1 >> 4, DEC);
      // Serial.print(", digit=");
      // Serial.println(data1 & 15, DEC);
      break;

    case usbMIDI.SongPosition: // 0xF2
      usbMIDI.sendSongPosition(data1 + data2 * 128, cable);
      // Serial.print("Song Position, beat=");
      // Serial.println(data1 + data2 * 128);
      break;

    case usbMIDI.SongSelect: // 0xF3
      usbMIDI.sendSongSelect(data1, cable);
      // Serial.print("Sond Select, song=");
      // Serial.println(data1, DEC);
      break;

    case usbMIDI.TuneRequest: // 0xF6
      usbMIDI.sendTuneRequest(cable);
      // Serial.println("Tune Request");
      break;

    case usbMIDI.Clock: // 0xF8
      usbMIDI.sendRealTime(usbMIDI.Clock, cable);
      // Serial.println("Clock");
      break;

    case usbMIDI.Start: // 0xFA
      usbMIDI.sendRealTime(usbMIDI.Start, cable);
      // Serial.println("Start");
      break;

    case usbMIDI.Continue: // 0xFB
      usbMIDI.sendRealTime(usbMIDI.Continue, cable);
      // Serial.println("Continue");
      break;

    case usbMIDI.Stop: // 0xFC
      usbMIDI.sendRealTime(usbMIDI.Stop, cable);
      // Serial.println("Stop");
      break;

    case usbMIDI.ActiveSensing: // 0xFE
      usbMIDI.sendRealTime(usbMIDI.ActiveSensing, cable);
      // Serial.println("Actvice Sensing");
      break;

    case usbMIDI.SystemReset: // 0xFF
      usbMIDI.sendRealTime(usbMIDI.SystemReset, cable);
      // Serial.println("System Reset");
      break;

    // default:
      // Serial.println("Opps, an unknown MIDI message type!");
  }
}