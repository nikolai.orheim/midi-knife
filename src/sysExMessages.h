#include <Arduino.h>

#ifndef SYSEX_MESSAGES_H
#define SYSEX_MESSAGES_H
typedef struct {
  uint8_t sysex_start;
  uint8_t manufacturer_id[3];
  uint8_t message_status;
  uint8_t message_part;
  uint8_t special_request;
  uint8_t sysex_stop;
} sysExPackageIn;

typedef struct {
  uint8_t manufacturer_id[3];
  uint8_t message_status;
  uint8_t message_part;
  uint8_t special_request;
} sysExPackageOut;
#endif
